const HDWalletProvider = require('@truffle/hdwallet-provider'); 
const Web3 = require('web3');
const { abi, evm } = require('./compile');

const provider = new HDWalletProvider(
    'hawk stadium safe seek little walk also garlic vicious banana online shoe',
    'https://goerli.infura.io/v3/e9e08835e9b94197bf89e757a243b906'
);

const web3 = new Web3(provider);


const deploy = async () => {
    const accounts = await web3.eth.getAccounts();

    const result = await new web3.eth.Contract(abi)
    .deploy({ data: evm.bytecode.object })
    .send({ from: accounts[0], gas: '1000000' });

    console.log(JSON.stringify(abi));
    console.log('Contract deployed to ', result.options.address);
    
    provider.engine.stop();     
};
deploy();
