# About

This is a Solidy lottery smart contract project.

A manager can deploy the contract, allow players to enter by sumitting some ether. The manager can then eventually pick a winner and reset the lottery.

# Testing

npm run test
